package employee.payroll.system;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//Configuration file: log4j2.xml
public class ApplicationLogger {
	static Logger logger=LogManager.getRootLogger();;

//	public ApplicationLogger() {
//		logger = LogManager.getRootLogger();
//	}

	public void setLevel(String s) {
		logger.fatal("This is a FATAL message.");
		logger.error("This is an ERROR message.");
		logger.warn("This is a WARN message.");
		logger.info("This is an INFO message.");
		logger.debug("This is a DEBUG message.");
		logger.trace("This is a TRACE message.");
	}

	public static Logger getApplicationLogger() {
		return logger;
	}

}
